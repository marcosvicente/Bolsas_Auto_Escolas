require "httparty"

namespace :utils do
  desc "all_states"
  task :all_states => :environment do
    response = HTTParty.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
    response.each do |json|
      state = State.create(name: json["nome"], acronym: json["sigla"], id: json["id"])
      state.save!
    end
  end
end
