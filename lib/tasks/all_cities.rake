namespace :utils do
  desc "all_cities"
  task :all_cities => :environment do
    @state = State.all
    @state.each do |state|
      url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/#{state.id}/municipios"
      puts url
      response = HTTParty.get(url)
      response.each do |json|
        city = City.create(name: json["nome"], state_id: state.id)
        city.save!
      end
    end
  end
end
