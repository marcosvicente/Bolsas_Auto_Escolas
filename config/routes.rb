Rails.application.routes.draw do
  get 'thanks/index'
  get 'registration/index'
  post 'registration/save_user'
  get 'driving_school/index'
  get 'clinical/index'
  get 'location/index'
  devise_for :users
  get 'home/index'
  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
