class LocationController < ApplicationController
  def index
    @license = params[:license]
    @long = params[:long]
    @lat = params[:lat]
  end
end
