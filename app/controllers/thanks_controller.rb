class ThanksController < ApplicationController

  def index
    @user = User.where(id: params[:user_id])
    @driving_school = DrivingSchool.where(id: @user[0]["driving_school_id"])
    @clinical = Clinical.where(id: @user[0]["clinical_id"])
    @city = City.where(id: @user[0]["city_id"])
    @state = State.where(id: @city[0]["state_id"] )
    @license = City.where(id: @user[0]["license_id"])

  end
  #  método export para chamar a lib que gera o PDF e depois redirecionar o usuário para baixo o PDF
 def export
   GeneratePdf::spending(User.all.map {|s| [s.section, s.value.to_f]})
   redirect_to '/spending.pdf'
 end
end
