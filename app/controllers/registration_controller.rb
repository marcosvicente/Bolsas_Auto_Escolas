class RegistrationController < ApplicationController
  def index
    @user = User.new
    @license = License.where(name: params[:license])
    @clinical = Clinical.where(id: params[:clinical])
    @driving_school = DrivingSchool.joins(:license).where(id: params[:driving_school])
    @city = City.where(id: @driving_school[0]["city_id"])
    @state = State.where(id: @city[0]["state_id"])
  end
  def save_user
    create_user
  end

  private
    def create_user
      @user = User.new(user_params)
      if @user.save!
        redirect_to thanks_index_path( user_id: @user.id)
        flash[:notice] = "Seu usuário foi criado."
      else
        flash[:notice] = "Seu usuário não foi criado."
      end

    end
    def user_params
      params.require(:user).permit(:name, :last_name, :email, :phone, :password, :driving_school_id, :clinical_id, :license_id, :city_id)
    end

end
