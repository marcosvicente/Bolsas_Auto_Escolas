class DrivingSchool < ApplicationRecord
  acts_as_taggable # Alias for acts_as_taggable_on :tags
  acts_as_taggable_on :skills, :interests
  belongs_to :city
  belongs_to :license

  # validates
  validates :name, :presence => true
  validates :price, :presence => true
  validates :address, :presence => true
  validates :logo, :presence => true
  validates :discount, :presence => false
end
