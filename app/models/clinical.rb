class Clinical < ApplicationRecord
  acts_as_taggable # Alias for acts_as_taggable_on :tags
  acts_as_taggable_on :skills, :interests
  belongs_to :city
  belongs_to :location

end
