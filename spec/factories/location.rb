require "faker"

FactoryBot.define do
  factory :location do
    lat {Faker::Address.latitude }
    long {Faker::Address.longitude }
    address {Faker::Address.full_address }
    name {Faker::Company.name }
  end

end
