FactoryBot.define do
  factory :signature do
    name {"pro"}
    price {Faker::Number.decimal(2)}
  end

end
