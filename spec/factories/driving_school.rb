FactoryBot.define do
  factory :driving_school do
    name {Faker::Company.name}
    city
    license
    discount {Faker::Number.decimal(3)}
    price {Faker::Number.decimal(3)}
    address {Faker::Address.street_address}
    logo{Faker::Avatar.image("my-own-slug", "50x50")}
    tag_list {Faker::Name.first_name}

  end
end
