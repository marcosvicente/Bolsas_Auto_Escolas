FactoryBot.define do
  factory :clinical do
    name {"Clinical" + Faker::Name.first_name}
    phone {Faker::PhoneNumber.cell_phone}
    tag_list {Faker::Name.first_name}
    location
    city
  end

end
