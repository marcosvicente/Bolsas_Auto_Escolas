Faker::Config.locale = 'pt-BR'

FactoryBot.define do
  factory :user do
    name {Faker::Name.name}
    last_name {Faker::Name.last_name}
    phone {Faker::PhoneNumber.phone_number}
    email {Faker::Internet.email}
    password Faker::Device.serial
  end

end
