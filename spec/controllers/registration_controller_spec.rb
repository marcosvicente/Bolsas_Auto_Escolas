require 'rails_helper'

RSpec.describe RegistrationController, type: :controller do
  describe " With out logged" do
    before(:each) do
      @user = create(:user)
      @user_attributes = attributes_for(:user)
    end
    context 'GET #index' do
      it "reponds with http 200" do
        get :index
        expect(response).to have_http_status(200)
      end
    end

    it "POST #save_user" do
      post :save_user, params:{ user: @user_attributes}
      expect(User.last.id).to eq(@user.id)

    end
  end
end
