require 'rails_helper'

RSpec.describe DrivingSchoolController, type: :controller do
  describe " With out logged" do
    context 'GET #index' do
      it "reponds with http 200" do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end
end
