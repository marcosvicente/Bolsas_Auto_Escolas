require 'rails_helper'

RSpec.describe License, type: :model do
  context "create a new license" do
    before(:each) do
      @license = create(:license)
    end
    it "create with valid arguments" do
      expect(License.count).to eq(1)
    end

  end

  context "not create a new license" do
    before(:each) do
      @invalid = build(:license, name: "Z")
    end
    it "create with not valid arguments" do
      expect(@invalid).to_not be_valid
    end
  end

end
