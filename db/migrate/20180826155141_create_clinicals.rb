class CreateClinicals < ActiveRecord::Migration[5.2]
  def change
    create_table :clinicals do |t|
      t.string :name
      t.string :address
      t.integer :phone
      t.string :open
      t.string :close
      t.references :city, foreign_key: true
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
