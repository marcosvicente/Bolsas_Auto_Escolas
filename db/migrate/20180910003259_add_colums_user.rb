class AddColumsUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :city, foreign_key: true
    add_reference :users, :driving_school, foreign_key: true
    add_reference :users, :license, foreign_key: true
    add_reference :users, :clinical, foreign_key: true

  end
end
