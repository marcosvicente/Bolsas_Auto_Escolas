class CreateDrivingSchools < ActiveRecord::Migration[5.2]
  def change
    create_table :driving_schools do |t|
      t.string :name
      t.references :city, foreign_key: true
      t.references :license, foreign_key: true
      t.decimal :price
      t.decimal :discount
      t.string :address
      t.string :logo
      t.string :open
      t.string :close
      t.timestamps
    end
  end
end
