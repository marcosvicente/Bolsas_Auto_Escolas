# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'
Faker::Config.locale = 'pt-BR'

License.create(name: 'A')
License.create(name: 'B')
License.create(name: 'C')
License.create(name: 'D')
License.create(name: 'AB')

for i in (0..10) do
  Location.create(lat: Faker::Address.latitude, long:Faker::Address.longitude)
end
for i in (0..10) do
  Clinical.create(
    name: Faker::Company.name,
    phone: Faker::PhoneNumber.cell_phone,
    address: Faker::Address.street_address,
    tag_list: Faker::Name.first_name,
    location_id: Location.last.id,
    city_id: City.last.id,
    open: "De Segunda à Sexta das 08:00 às 12:00 e das 14:00 às 18:00",
    close: "Sábados, Domingos e Feriados fechado.")
end
for i in (0..10) do
  DrivingSchool.create(name: Faker::Company.name,
     city_id: City.last.id,
      discount: Faker::Number.decimal(3),
      license_id: License.last.id,
      price: Faker::Number.decimal(3),
      address: Faker::Address.street_address,
      logo: Faker::Avatar.image("my-own-slug", "50x50"),
      tag_list: Faker::Name.first_name,
      open: "De Segunda à Sexta das 08:00 às 12:00 e das 14:00 às 18:00",
      close: "Sábados, Domingos e Feriados fechado."
    )
end
